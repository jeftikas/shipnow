# Sistema de archivos en consola

Este proyecto implementa un sistema de archivos básico en consola, donde puedes crear archivos, carpetas, navegar entre ellas y ejecutar comandos relacionados con la administración de archivos y carpetas.

## Funcionalidades

El sistema de archivos en consola proporciona las siguientes funcionalidades:

- Crear archivos: Puedes crear nuevos archivos en la carpeta actual.
- Mostrar contenido de un archivo: Puedes ver el contenido de un archivo existente.
- Mostrar metadatos de un archivo: Puedes ver los metadatos asociados a un archivo.
- Crear carpetas: Puedes crear nuevas carpetas dentro de la carpeta actual.
- Cambiar de directorio: Puedes navegar entre las carpetas existentes y moverte a la carpeta padre utilizando el comando `cd ..`.
- Listar contenido: Puedes ver los archivos y carpetas existentes en la carpeta actual.
- Eliminar archivos: Puedes eliminar archivos existentes.
- Mostrar la ubicación actual: Puedes ver la ruta de la carpeta en la que te encuentras.

## Instrucciones de uso

1. Ejecuta el archivo `terminal.js` utilizando Node.js en tu terminal.
2. Se te mostrará el prompt `>`, lo cual indica que puedes ingresar comandos.
3. Puedes ingresar los siguientes comandos para interactuar con el sistema de archivos:
   - `create_file <nombre>`: Crea un nuevo archivo con el nombre especificado.
   - `show <nombre>`: Muestra el contenido de un archivo existente.
   - `metadata <nombre>`: Muestra los metadatos de un archivo existente.
   - `create_folder <nombre>`: Crea una nueva carpeta con el nombre especificado.
   - `cd <nombre>`: Cambia al directorio especificado.
   - `cd ..`: Regresa al directorio padre.
   - `ls`: Lista el contenido de la carpeta actual.
   - `destroy <nombre>`: Elimina un archivo existente.
   - `whereami`: Muestra la ubicación actual en la estructura de carpetas.
4. Puedes ingresar `ctrl + c` para salir del sistema de archivos en consola.

¡Disfruta explorando y administrando tu sistema de archivos en consola!
