// Clase para representar un archivo
let File = class {
  constructor(name, content) {
    this.name = name;
    this.content = content;
    this.metadata = {};
  }
}

// Clase para representar una carpeta
let Folder= class {
  constructor(name, parent = null) {
    this.name = name;
    this.files = [];
    this.subfolders = [];
    this.metadata = {};
    this.parent = parent;
  }

  addFile(file) {
    this.files.push(file);
  }

  addSubfolder(subfolder) {
    this.subfolders.push(subfolder);
  }
}

// Clase para representar la consola
let Terminal= class{
  constructor() {
    this.currentFolder = new Folder("root");

  }

  runCommand(command) {
    const [action, target] = command.split(" ");

    switch (action) {
      case "create_file":
        this.createFile(target);
        break;
      case "show":
        this.show(target);
        break;
      case "metadata":
        this.metadata(target);
        break;
      case "create_folder":
        this.createFolder(target);
        break;     
      case "cd":
        this.changeDirectory(target);
        break;
      case "cd ..":
        this.changeDirectory(target);
        break;
      case "ls":
        this.listContent(target);
        break;
      case "destroy":
        this.destroy(target);
        break;
      case "whereami":
        this.whereami(target);
        break;


      default:
        console.log("Comando no reconocido.");
    }
  }

  createFile(name) {
    const file = new File(name, "");
    this.currentFolder.addFile(file);
    console.log(`Archivo "${name}" creado.`);
  }

  show(name) {
    const file = this.currentFolder.files.find(
      (file) => file.name === name
    );

    if (file) {
      console.log(file.content);
    } else {
      console.log(`El archivo "${name}" no existe.`);
    }
  }

  metadata(name) {
    const file = this.currentFolder.files.find(
      (file) => file.name === name
    );
      
    if (file) {
      console.log(file.metadata);
    } else {
      console.log(`El archivo "${name}" no existe.`);
    }
  }


  createFolder(name) {
    const folder = new Folder(name);
    this.currentFolder.addSubfolder(folder);
    console.log(`Carpeta "${name}" creada.`);
  } 

  changeDirectory(folderName) {
    if (folderName === "..") {
      // Regresar a la carpeta padre si el comando es ".."
      if (this.currentFolder.parent) {
        this.currentFolder = this.currentFolder.parent;
        console.log("Regresaste a la carpeta padre.");
      } else {
        console.log("Ya estás en la carpeta raíz.");
      }
    } else {
      // Ingresar a la subcarpeta correspondiente
      const folder = this.currentFolder.subfolders.find(
        (folder) => folder.name === folderName
      );

      if (folder) {
        this.currentFolder = folder;
        console.log(`Ingresaste a la carpeta "${folderName}".`);
      } else {
        console.log(`La carpeta "${folderName}" no existe.`);
      }
    }
  }

  listContent() {
    console.log("Archivos:");
    this.currentFolder.files.forEach((file) => {
      console.log(`- ${file.name}`);
    });

    console.log("Carpetas:");
    this.currentFolder.subfolders.forEach((folder) => {
      console.log(`- ${folder.name}`);
    });
  }
  


  destroy(name) {
    const fileIndex = this.currentFolder.files.findIndex(
      (file) => file.name === name
    );

    if (fileIndex >= 0) {
      this.currentFolder.files.splice(fileIndex, 1);
      console.log(`El archivo "${name}" fue eliminado.`);
    } else {
      console.log(`El archivo "${name}" no existe.`);
    }
  }

  whereami() {
    const path = this.getPath(this.currentFolder);
    console.log(`Estás en la ruta "/${path}".`);
  }
  
  getPath(folder) {
    if (folder.parent) {
      return this.getPath(folder.parent) + "/" + folder.name;
    }
    return folder.name;
  }




}

const readline = require('readline');

const terminal = new Terminal();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.setPrompt('> ');
rl.prompt();

rl.on('line', (input) => {
  terminal.runCommand(input.trim());
  rl.prompt();
});

rl.on('close', () => {
  console.log('¡Adiós!');
  process.exit(0);
});